---
title: "안녕하세요 😛"
date: 2021-06-07T00:21:30+09:00
draft: false
disable_comments: true
categories:
  - blog
tags:
  - blog
featuredImage: /images/first-post/first-post.png
images:
  -  /images/first-post/first-post.png
---

# 안녕하세요 :) 
> 블로그 시작할꺼에요

필자는 평소 개발자에게 필요한 곳이 블로그라 생각하고 있다. <br>
글쓰기와 접점을 고민하는 장소!
